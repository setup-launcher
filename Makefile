##################################################################################################
# Setup Bootstrapper Utility                                                                     #
# Copyright(c) 2016-2020 LoRd_MuldeR <mulder2@gmx.de>                                            #
#                                                                                                #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software  #
# and associated documentation files (the "Software"), to deal in the Software without           #
# restriction, including without limitation the rights to use, copy, modify, merge, publish,     #
# distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the  #
# Software is furnished to do so, subject to the following conditions:                           #
#                                                                                                #
# The above copyright notice and this permission notice shall be included in all copies or       #
# substantial portions of the Software.                                                          #
#                                                                                                #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING  #
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND     #
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   #
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, #
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.        #
##################################################################################################

APPNAME := setup-launcher

OUTPATH_EXE := out/$(APPNAME).exe
OUTPATH_DLL := out/$(APPNAME).dll
RESPATH_EXE := res/$(APPNAME).exe.o
RESPATH_DLL := res/$(APPNAME).dll.o

BUILD_TIME := $(shell date +'%s')
BUILD_DATE := \
	-DBUILD_DATE_X=$(shell printf '%08X' $(BUILD_TIME)) \
	-DBUILD_DATE_F=$(shell date --date='@$(BUILD_TIME)' +'%Y-%m-%d') \
	-DBUILD_DATE_Y=$(shell date --date='@$(BUILD_TIME)' +'%-Y') \
	-DBUILD_DATE_M=$(shell date --date='@$(BUILD_TIME)' +'%-m') \
	-DBUILD_DATE_D=$(shell date --date='@$(BUILD_TIME)' +'%-d')

.PHONY: all clean

all: $(OUTPATH_EXE) $(OUTPATH_DLL)

$(OUTPATH_EXE): setup-launcher.c $(RESPATH_EXE)
	mkdir -p $(dir $@)
	gcc -O2 -static -DBUILD_EXE $(BUILD_DATE) -o $@ -mwindows -municode $?
	for i in {1..5}; do strip $@ && break; done

$(OUTPATH_DLL): setup-launcher.c $(RESPATH_DLL) setup-launcher.def
	mkdir -p $(dir $@)
	gcc -O2 -shared -DBUILD_DLL $(BUILD_DATE) -o $@ -mwindows -Wl,--enable-stdcall-fixup $?
	for i in {1..5}; do strip $@ && break; done

$(RESPATH_EXE): resources.rc
	mkdir -p $(dir $@)
	windres -DBUILD_EXE $(BUILD_DATE) -o $@ $<

$(RESPATH_DLL): resources.rc
	mkdir -p $(dir $@)
	windres -DBUILD_DLL $(BUILD_DATE) -o $@ $<

clean:
	rm -vf $(dir $(RESPATH_EXE))*.o
	rm -vf $(dir $(OUTPATH_EXE))*.{exe,dll}
