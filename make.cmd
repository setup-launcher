@echo off
cd /d "%~dp0"

if not exist "%MSYS2_HOME%\msys2_shell.cmd" (
	echo MSYS2 shell not found. Please check MSYS2_HOME and try again!
	pause
	goto:eof
)

call "%MSYS2_HOME%\msys2_shell.cmd" -mingw32 -no-start -defterm -where "%~dp0" -c "make clean && make -B"

pause
