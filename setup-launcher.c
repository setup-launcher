/* ---------------------------------------------------------------------------------------------- */
/* Setup Bootstrapper Utility                                                                     */
/* Copyright(c) 2016-2020 LoRd_MuldeR <mulder2@gmx.de>                                            */
/*                                                                                                */
/* Permission is hereby granted, free of charge, to any person obtaining a copy of this software  */
/* and associated documentation files (the "Software"), to deal in the Software without           */
/* restriction, including without limitation the rights to use, copy, modify, merge, publish,     */
/* distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the  */
/* Software is furnished to do so, subject to the following conditions:                           */
/*                                                                                                */
/* The above copyright notice and this permission notice shall be included in all copies or       */
/* substantial portions of the Software.                                                          */
/*                                                                                                */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING  */
/* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND     */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,   */
/* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, */
/* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.        */
/* ---------------------------------------------------------------------------------------------- */

#if !defined(BUILD_EXE) && !defined(BUILD_DLL)
#error Nothing to do!
#endif
#if defined(BUILD_EXE) && defined(BUILD_DLL)
#error Inconsistent build flags!
#endif
#if !defined(BUILD_DATE_F) || !defined(BUILD_DATE_X)
#error Build date is not defined!
#endif

#include <Windows.h>
#include <wchar.h>
#include <stdarg.h>

// ==========================================================================
// COMMON FUNCTIONS
// ==========================================================================

#define MAX_STRLEN 32768U

#define _STRINGIFY(X) L###X
#define STRINGIFY(X) _STRINGIFY(X)

#define _HEXLIFY(X) 0x##X
#define HEXLIFY(X) _HEXLIFY(X)

#define RECT_W(X) (((X).right > (X).left) ? ((X).right - (X).left) : 0)
#define RECT_H(X) (((X).bottom > (X).top) ? ((X).bottom - (X).top) : 0)

#define TITLE(STR) \
({ \
	const wchar_t *const _title = (STR); \
	(_title && (*_title)) ? _title : PROGRAM_NAME; \
})

static const wchar_t *const PROGRAM_NAME = L"Setup Bootstrapper";
static const unsigned long API_VERSION = HEXLIFY(BUILD_DATE_X);

// ----------------------------------------------------
// String Functions
// ----------------------------------------------------

/*Note: Cannot use wcsncpy_s() or wcsncat_s(), because they were NOT available in Windows XP yet!*/

#define EMPTY(STR) ((!(STR)) || (!(*(STR))))

#define ENSURE_NULL_TERMINATED(BUFF,SIZE) do \
{ \
	(BUFF)[(SIZE) - 1U] = L'\0'; \
} \
while(0)

static wchar_t *copy(wchar_t *const buffer, const wchar_t *const str, const size_t buff_size)
{
	wcsncpy(buffer, str, buff_size);
	ENSURE_NULL_TERMINATED(buffer, buff_size); /*not guranteed by wcsncpy()!*/
	return buffer;
}

static wchar_t *append(wchar_t *const buffer, const wchar_t *const str, const size_t buff_size)
{
	const size_t current_len = wcslen(buffer);
	if(current_len < (buff_size - 1U))
	{
		wcsncat(buffer, str, buff_size - current_len - 1U);
	}
	return buffer;
}

static wchar_t *format(wchar_t *const buffer, const size_t buff_size, const wchar_t *const format, ...)
{
	va_list args;
	va_start(args, format);
	_vsnwprintf(buffer, buff_size, format, args);
	va_end(args);
	ENSURE_NULL_TERMINATED(buffer, buff_size); /*not guranteed by _vsnwprintf()!*/
	return buffer;
}

static wchar_t *append_format(wchar_t *const buffer, const size_t buff_size, const wchar_t *const format, ...)
{
	const size_t current_len = wcslen(buffer);
	if(current_len < (buff_size - 1U))
	{
		va_list args;
		va_start(args, format);
		_vsnwprintf(buffer + current_len, buff_size - current_len, format, args);
		va_end(args);
		ENSURE_NULL_TERMINATED(buffer, buff_size); /*not guranteed by _vsnwprintf()!*/
	}
	return buffer;
}

static size_t max_index(const size_t pos_a, const size_t pos_b)
{
	if((pos_a != SIZE_MAX) && (pos_b != SIZE_MAX))
	{
		return pos_a > pos_b ? pos_a : pos_b;
	}
	return (pos_a != SIZE_MAX) ? pos_a : pos_b;
}

static size_t last_index(const wchar_t *const haystack, const wchar_t needle)
{
	const wchar_t *ptr;
	if(ptr = wcsrchr(haystack, needle))
	{
		return ptr - haystack;
	}
	return SIZE_MAX;
}

static BOOL contains_space(const wchar_t *str)
{
	for(; *str != L'\0'; ++str)
	{
		if(iswspace(*str))
		{
			return TRUE;
		}
	}
	return FALSE;
}

static BOOL starts_with(const wchar_t *const str, const wchar_t *const prefix)
{
	if(*str)
	{
		const size_t len_str = wcslen(str), len_prefix = wcslen(prefix);
		if(len_str >= len_prefix)
		{
			return (_wcsnicmp(str, prefix, len_prefix) == 0);
		}
	}
	return FALSE;
}

// ----------------------------------------------------
// Command-line Functions
// ----------------------------------------------------

#define TRY_APPEND(OUT,LEN,MAX_LEN,CHR) do \
{ \
	if((OUT) && ((LEN) < ((MAX_LEN) - 1U))) \
	{ \
		(OUT)[(LEN)++] = (CHR); \
	} \
} \
while(0)

static const wchar_t *get_next_arg(const wchar_t *cmdLine, wchar_t *const arg_out, const size_t max_len)
{
	size_t len = 0U, escape_count = 0U;
	BOOL flag_quote = FALSE;
	
	/*skip leading spaces*/
	while ((*cmdLine != L'\0') && iswspace(*cmdLine))
	{
		++cmdLine;
	}
	
	/*process token*/
	for (; *cmdLine != L'\0'; ++cmdLine)
	{
		if (*cmdLine == L'\\')
		{
			++escape_count;
			continue;
		}
		else if (escape_count)
		{
			if (*cmdLine == L'"')
			{
				const size_t modul = escape_count % 2U;
				for (escape_count /= 2U; escape_count; --escape_count)
				{
					TRY_APPEND(arg_out, len, max_len, L'\\');
				}
				if (!modul)
				{
					flag_quote = (!flag_quote);
					continue;
				}
			}
			else
			{
				for (; escape_count; --escape_count)
				{
					TRY_APPEND(arg_out, len, max_len, L'\\');
				}
			}
		}
		else if (*cmdLine == L'"')
		{
			flag_quote = (!flag_quote);
			continue;
		}
		if ((!flag_quote) && iswspace(*cmdLine))
		{
			break; /*stop!*/
		}
		TRY_APPEND(arg_out, len, max_len, *cmdLine);
	}
	
	/*flush pending backslashes*/
	for (; escape_count; --escape_count)
	{
		TRY_APPEND(arg_out, len, max_len, L'\\');
	}
	
	/*skip trailing spaces*/
	while ((*cmdLine != L'\0') && iswspace(*cmdLine))
	{
		++cmdLine;
	}
	
	/*put the NULL terminator*/
	if (arg_out)
	{
		arg_out[len] = L'\0';
	}
	
	return cmdLine;
}

// ----------------------------------------------------
// Path Functions
// ----------------------------------------------------

static BOOL get_executable_path(wchar_t *const path, const size_t max_len)
{
	const DWORD ret = GetModuleFileNameW(NULL, path, max_len);
	if((ret > 0U) && (ret < max_len))
	{
		return TRUE;
	}
	else
	{
		get_next_arg(GetCommandLineW(), path, max_len);
		return (!EMPTY(path));
	}
}

static BOOL get_system_path(wchar_t *const path, const size_t max_len)
{
	const DWORD ret = GetSystemDirectoryW(path, max_len);
	if((ret > 0U) || (ret < max_len))
	{
		return TRUE;
	}
	return FALSE;
}

static wchar_t *skip_directory_part(wchar_t *const path)
{
	const size_t pos_sep = max_index(last_index(path, L'/'), last_index(path, L'\\'));
	if (pos_sep != SIZE_MAX)
	{
		return path + (pos_sep + 1U);
	}
	return path;
}

static void get_absolute_path(const wchar_t *const path, wchar_t *const full_path, const size_t max_len, const wchar_t **const file_name)
{
	const DWORD ret = GetFullPathNameW(path, max_len, full_path, (LPWSTR*)file_name);
	if(!((ret > 0U) && (ret < max_len)))
	{
		copy(full_path, path, max_len);
		if(file_name)
		{
			*file_name = skip_directory_part(full_path);
		}
	}
}

static wchar_t *remove_file_name(wchar_t *const path)
{
	wchar_t *const file_name = skip_directory_part(path);
	if(*file_name)
	{
		*file_name = L'\0';
	}
	return path;
}

static wchar_t *remove_file_extension(wchar_t *const path)
{
	wchar_t *const file_name = skip_directory_part(path);
	if(*file_name)
	{
		const size_t pos_ext = last_index(file_name, L'.');
		if((pos_ext != SIZE_MAX) && (pos_ext > 0U))
		{
			file_name[pos_ext] = L'\0';
		}
	}
	return path;
}

static wchar_t *trim_trailing_separators(wchar_t *const path, const BOOL force)
{
	size_t length = wcslen(path);
	while ((length > (force ? 0U : 1U)) && ((path[length - 1U] == L'/') || (path[length - 1U] == L'\\')))
	{
		if((!force) && path[length - 2U] == L':')
		{
			break;
		}
		path[--length] = L'\0';
	}
	return path;
}

static BOOL file_exists(const wchar_t *const path)
{
	const DWORD attributes = GetFileAttributesW(path);
	return ((attributes != INVALID_FILE_ATTRIBUTES) && (!(attributes & FILE_ATTRIBUTE_DIRECTORY)));
}

// ----------------------------------------------------
// ----------------------------------------------------
// UAC Functions
// ----------------------------------------------------

static BOOL is_elevated(void)
{
	const DWORD version = GetVersion();
	if(((DWORD)LOBYTE(LOWORD(version))) > 5U)
	{
		BOOL elevated = FALSE;
		HANDLE hToken;
		if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
		{
			DWORD cbSize = sizeof(TOKEN_ELEVATION_TYPE);
			TOKEN_ELEVATION_TYPE elevation_type;
			if(GetTokenInformation(hToken, TokenElevationType, &elevation_type, sizeof(TOKEN_ELEVATION_TYPE), &cbSize))
			{
				switch(elevation_type)
				{
				case TokenElevationTypeFull:
				case TokenElevationTypeDefault:
					elevated = TRUE;
					break;
				}
			}
			CloseHandle(hToken);
		}
		return elevated;
	}
	else
	{
		return TRUE; /*for Windows XP support*/
	}
}

// ----------------------------------------------------
// Window Functions
// ----------------------------------------------------

#define IDI_ICON1 101

#define PROCESS_MESSAGES(X) do \
{ \
	const HWND _hwnd = (X); \
	if(_hwnd) \
		process_messages(_hwnd); \
} \
while(0)

static HWND create_banner(const HINSTANCE hInstance, const wchar_t *const title, WNDPROC wndproc)
{
	HWND hwnd = CreateWindowExW(0, L"#32770", TITLE(title), WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT, 384, 96, NULL, NULL, hInstance, NULL);
	if (hwnd != NULL)
	{
		RECT workRect, wndRect;
		HICON hIcon;
		SetWindowLongPtrW(hwnd, GWLP_WNDPROC,  (LONG_PTR)wndproc);
		SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
		if (SystemParametersInfoW(SPI_GETWORKAREA, 0, &workRect, 0) && GetWindowRect(hwnd, &wndRect))
		{
			MoveWindow(hwnd, (RECT_W(workRect)-RECT_W(wndRect))/2, (RECT_H(workRect)-RECT_H(wndRect))/2, RECT_W(wndRect), RECT_H(wndRect), TRUE);
		}
		if((hIcon = (HICON)LoadImageW(hInstance, MAKEINTRESOURCEW(IDI_ICON1), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR | LR_SHARED)) != NULL)
		{
			SendMessage(hwnd, WM_SETICON, (WPARAM)ICON_SMALL, (LPARAM)hIcon);
		}
		if((hIcon = (HICON)LoadImageW(hInstance, MAKEINTRESOURCEW(IDI_ICON1), IMAGE_ICON, 48, 48, LR_DEFAULTCOLOR | LR_SHARED)) != NULL)
		{
			SendMessage(hwnd, WM_SETICON, (WPARAM)ICON_BIG, (LPARAM)hIcon);
		}
	}
	return hwnd;
}

static void process_messages(const HWND hwnd)
{
	MSG msg = {};
	for (unsigned short i = 0U; i < 8U; ++i)
	{
		BOOL flag = FALSE;
		if(i > 0U)
		{
			Sleep(1); /*delay*/
		}
		for (unsigned short k = 0U; k < 8192U; ++k)
		{
			if (PeekMessageW(&msg, hwnd, 0U, 0U, PM_REMOVE))
			{
				flag = TRUE;
				TranslateMessage(&msg);
				DispatchMessageW(&msg);
				continue;
			}
			break;
		}
		if(!flag)
		{
			break; /*did not process any messages*/
		}
	}
}

static LRESULT CALLBACK wnd_proc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	switch(uMsg)
	{
	case WM_PAINT:
		if(hdc = BeginPaint(hwnd, &ps))
		{
			FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));
			DrawTextExW(hdc, L"Setup is launching, please stay tuned...", -1, &ps.rcPaint, DT_CENTER|DT_SINGLELINE|DT_VCENTER, NULL);
			EndPaint(hwnd, &ps);
		}
	case WM_CLOSE:
		return 0; /*ignore WM_CLOSE msg*/
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
}

static BOOL show_window(const HWND hwnd)
{
	BOOL result = FALSE;
	if(hwnd != NULL)
	{
		result = ShowWindow(hwnd, SW_SHOWNORMAL);
		process_messages(hwnd);
		Sleep(25);
		SetForegroundWindow(hwnd);
		SetCapture(hwnd);
	}
	return result;
}

// ----------------------------------------------------
// Shell Execute
// ----------------------------------------------------

static BOOL exec_shell(const HWND hwnd, const wchar_t *const verb, const wchar_t *const file, const wchar_t *const params, const wchar_t *const directory, HANDLE *const process)
{
	SHELLEXECUTEINFOW shExecInfo;
	SecureZeroMemory(&shExecInfo, sizeof(SHELLEXECUTEINFOW));
	shExecInfo.cbSize = sizeof(SHELLEXECUTEINFOW);
	shExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExecInfo.hwnd = hwnd;
	shExecInfo.lpVerb = verb;
	shExecInfo.lpFile = file;
	shExecInfo.lpParameters = params;
	shExecInfo.lpDirectory = directory;
	shExecInfo.nShow = SW_SHOWNORMAL;
	const BOOL success = ShellExecuteExW(&shExecInfo);
	if(success && (((UINT32)(UINT_PTR)shExecInfo.hInstApp) > 32U))
	{
		if(process)
		{
			*process = shExecInfo.hProcess;
		}
		else
		{
			if(shExecInfo.hProcess != NULL)
			{
				CloseHandle(shExecInfo.hProcess);
			}
		}
		return TRUE;
	}
	else
	{
		if(process)
		{
			*process = NULL;
		}
		return FALSE;
	}
}

static DWORD exec_shell_await(HWND *const hwnd, const wchar_t *const verb, const wchar_t *const file_name, const wchar_t *const full_path, const wchar_t *const params, const wchar_t *const directory)
{
	HANDLE process = NULL;
	for(;;)
	{
		const HWND owner = hwnd ? (*hwnd) : NULL;
		PROCESS_MESSAGES(owner);
		if(!exec_shell(owner, verb, full_path, params, directory, &process))
		{
			PROCESS_MESSAGES(owner);
			if(MessageBoxW(owner, L"Failed to launch setup program. Please try again!", TITLE(file_name), (owner ? MB_TOPMOST : MB_SYSTEMMODAL) | MB_ICONEXCLAMATION | MB_RETRYCANCEL) != IDRETRY)
			{
				return 0x2E4;
			}
		}
		else
		{
			DWORD exit_code = 0U;
			Sleep(25);
			if(hwnd && (*hwnd))
			{
				DestroyWindow(*hwnd);
				*hwnd = NULL;
			}
			if(process)
			{
				WaitForSingleObject(process, INFINITE);
				if(!GetExitCodeProcess(process, &exit_code))
				{
					exit_code = (DWORD)(-1);
				}
				CloseHandle(process);
			}
			return exit_code; /*success*/
		}
	}
}

// ----------------------------------------------------
// Miscellaneous
// ----------------------------------------------------

static void error_message(const HWND hwnd, const wchar_t *const message, const wchar_t *const title)
{
	MessageBoxW(hwnd, message, TITLE(title), (hwnd ? MB_TOPMOST : MB_SYSTEMMODAL) | MB_ICONERROR);
}

static void show_help(const HWND hwnd, const wchar_t *const executable_name)
{
	wchar_t message[256U];
	format(message, 256U, L"Usage:\n\n%s <setup.exe> [PARAMETERS]", executable_name);
	MessageBoxW(hwnd, message, PROGRAM_NAME, (hwnd ? MB_TOPMOST : MB_SYSTEMMODAL) | MB_ICONINFORMATION);
}

static void show_about(const HWND hwnd, const BOOL dll)
{
	wchar_t message[256U];
	format(message, 256U, L"%s %s [%s]\n"
		L"Copyright(c) 2016-2020 LoRd_MuldeR <mulder2@gmx.de>\n"
		L"Please see http://muldersoft.com/ for details!\n\n"
		L"This program is free software; you can redistribute it and/or modify it under the terms of the the MIT License.",
		PROGRAM_NAME, dll ? L"DLL" : L"EXE", STRINGIFY(BUILD_DATE_F));
	MessageBoxW(hwnd, message, L"About...", (hwnd ? MB_TOPMOST : MB_SYSTEMMODAL) | MB_ICONINFORMATION);
}

// ==========================================================================
// EXE File
// ==========================================================================

#ifdef BUILD_EXE

static wchar_t executable_path     [MAX_STRLEN] = { L'\0' };
static wchar_t executable_directory[MAX_STRLEN] = { L'\0' };
static wchar_t setup_path          [MAX_STRLEN] = { L'\0' };
static wchar_t setup_absolute_path [MAX_STRLEN] = { L'\0' };
static wchar_t setup_directory     [MAX_STRLEN] = { L'\0' };
static wchar_t library_path        [MAX_STRLEN] = { L'\0' };
static wchar_t rundll32_path       [MAX_STRLEN] = { L'\0' };
static wchar_t parameters          [MAX_STRLEN] = { L'\0' };

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR pCmdLine, int nCmdShow)
{
	/*get executable path*/
	if(!get_executable_path(executable_path, MAX_STRLEN))
	{
		error_message(NULL, L"Error: Failed to determine executable path!", NULL);
		return 1;
	}

	/*get command-line arguments*/
	if(EMPTY(pCmdLine))
	{
		error_message(NULL, L"Required command-line parameters are missing!", NULL);
		show_help(NULL, skip_directory_part(executable_path));
		return 1;
	}

	/*show about screen*/
	if((!_wcsicmp(pCmdLine, L"--about")) || (!_wcsicmp(pCmdLine, L"--version")))
	{
		show_about(NULL, FALSE);
		return 0;
	}

	/*show help screen*/
	if((!_wcsicmp(pCmdLine, L"--help")) || (!_wcsicmp(pCmdLine, L"/?")) || (!_wcsicmp(pCmdLine, L"-?")))
	{
		show_help(NULL, skip_directory_part(executable_path));
		return 0;
	}

	/*get executable directory*/
	copy(executable_directory, executable_path, MAX_STRLEN);
	remove_file_name(executable_directory);
	trim_trailing_separators(executable_directory, FALSE);

	/*set working directory*/
	SetCurrentDirectoryW(executable_directory);

	/*get setup file path*/
	const wchar_t *const cmd_args = get_next_arg(pCmdLine, setup_path, MAX_STRLEN);
	if(EMPTY(setup_path))
	{
		error_message(NULL, L"Error: Failed to determine setup program path!", NULL);
	}

	/*get absolute path file path*/
	const wchar_t *file_name;
	get_absolute_path(setup_path, setup_absolute_path, MAX_STRLEN, &file_name);
	
	/*check for silent mode*/
	const BOOL silent = starts_with(cmd_args, L"-ms") || starts_with(cmd_args, L"/ini") || starts_with(cmd_args, L"/s");

	/*create banner window*/
	HWND banner = (!silent) ? create_banner(hInstance, file_name, wnd_proc) : NULL;
	if(banner != NULL)
	{
		show_window(banner);
	}
	
	/*make sure the setup executable exists*/
	if(!file_exists(setup_absolute_path))
	{
		format(parameters, MAX_STRLEN, L"Setup executable could not be found:\n\n%s", setup_absolute_path);
		error_message(banner, parameters, file_name);
		return 0x490;
	}
	
	/*get setup file directory*/
	copy(setup_directory, setup_absolute_path, MAX_STRLEN);
	remove_file_name(setup_directory);
	trim_trailing_separators(setup_directory, FALSE);
	
	/*set working directory*/
	SetCurrentDirectoryW(setup_directory);
	
	/*take the fast path, iff already elevated (or running on pre-Vista)*/
	if(is_elevated())
	{
		return exec_shell_await(&banner, NULL, file_name, setup_absolute_path, cmd_args, setup_directory);
	}
	
	/*get path of RunDLL executable file*/
	if(!get_system_path(rundll32_path, MAX_STRLEN))
	{
		error_message(banner, L"Error: Failed to determine system directory path!", file_name);
		return 1;
	}
	trim_trailing_separators(rundll32_path, TRUE);
	append(rundll32_path, L"\\rundll32.exe", MAX_STRLEN);
	
	/*make sure the RunDLL executable exists*/
	if(!file_exists(rundll32_path))
	{
		format(parameters, MAX_STRLEN, L"RUNDLL32 program could not be found:\n\n%s\n\n\nThis indicates that your Windows installation is corrupted!", rundll32_path);
		error_message(banner, parameters, file_name);
		return 0x485;
	}
	
	/*get path of launcher DLL*/
	copy(library_path, executable_path, MAX_STRLEN);
	remove_file_extension(library_path);
	append(library_path, L".dll", MAX_STRLEN);
	
	/*make sure the launcher DLL exists*/
	if(!file_exists(library_path))
	{
		format(parameters, MAX_STRLEN, L"Launcher library file could not be found:\n\n%s", library_path);
		error_message(banner, parameters, file_name);
		return 0x485;
	}
	
	/*build parameter string for RunDLL*/
	format(parameters, MAX_STRLEN, contains_space(library_path) ? L"\"%s\",startup" : L"%s,startup", library_path);
	append_format(parameters, MAX_STRLEN, contains_space(setup_absolute_path) ? L"\x20%X\x20\"%s\"" : L"\x20%X\x20%s", API_VERSION, setup_absolute_path);
	if(!EMPTY(cmd_args))
	{
		append_format(parameters, MAX_STRLEN, L"\x20%s", cmd_args);
	}
	
	/*now actually call the launcher DLL*/
	const DWORD exit_code = exec_shell_await(&banner, L"runas", file_name, rundll32_path, parameters, setup_directory);
	
	/*clean-up*/
	if(banner != NULL)
	{
		DestroyWindow(banner);
	}
	
	return exit_code;
}

#endif //BUILD_EXE

// ==========================================================================
// DLL File
// ==========================================================================

#ifdef BUILD_DLL

static wchar_t setup_path         [MAX_STRLEN] = { L'\0' };
static wchar_t setup_absolute_path[MAX_STRLEN] = { L'\0' };
static wchar_t setup_directory    [MAX_STRLEN] = { L'\0' };

static HINSTANCE g_hinstDLL = NULL;

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if(fdwReason == DLL_PROCESS_ATTACH)
	{
		g_hinstDLL = hinstDLL;
	}
	return TRUE;
}

void CALLBACK startupW(HWND hwnd, HINSTANCE hAppInstance, LPCWSTR lpCmdLine, int nCmdShow)
{
	/*check command-line args*/
	if(EMPTY(lpCmdLine))
	{
		error_message(hwnd, L"Required command-line parameters are missing!", NULL);
		ExitProcess(0x57);
		return;
	}
	
	/*get the version tag*/
	lpCmdLine = get_next_arg(lpCmdLine, setup_path, MAX_STRLEN); /*use 'setup_path' to store version tag!*/
	if(EMPTY(setup_path))
	{
		error_message(hwnd, L"Error: Failed to determine the version tag!", NULL);
		ExitProcess(0x57);
		return;
	}
	
	/*check the version tag*/
	if(wcstoul(setup_path, NULL, 16U) != API_VERSION)
	{
		error_message(hwnd, L"Error: Version tag mismatch detected!", NULL);
		ExitProcess(0x57);
		return;
	}
	
	/*check remaining command-line arguments*/
	if(EMPTY(lpCmdLine))
	{
		error_message(hwnd, L"Required command-line parameters are missing!", NULL);
		ExitProcess(0x57);
		return;
	}
	
	/*get setup file path*/
	lpCmdLine = get_next_arg(lpCmdLine, setup_path, MAX_STRLEN);
	if(EMPTY(setup_path))
	{
		error_message(hwnd, L"Error: Failed to determine setup program path!", NULL);
		ExitProcess(0x57);
		return;
	}
	
	/*get absolut setup file path*/
	const wchar_t *file_name;
	get_absolute_path(setup_path, setup_absolute_path, MAX_STRLEN, &file_name);
	
	/*check for silent mode*/
	const BOOL silent = starts_with(lpCmdLine, L"-ms") || starts_with(lpCmdLine, L"/ini") || starts_with(lpCmdLine, L"/s");

	/*create banner window*/
	HWND banner = (!silent) ? create_banner(g_hinstDLL, file_name, wnd_proc) : NULL;
	if(banner != NULL)
	{
		show_window(banner);
	}
	
	/*make sure the setup executable exists*/
	if(!file_exists(setup_absolute_path))
	{
		format(setup_directory, MAX_STRLEN, L"Setup executable could not be found:\n\n%s", setup_absolute_path);
		error_message((banner != NULL) ? banner : hwnd, setup_directory, file_name);
		ExitProcess(0x490);
		return;
	}
	
	/*get setup file directory*/
	copy(setup_directory, setup_absolute_path, MAX_STRLEN);
	remove_file_name(setup_directory);
	trim_trailing_separators(setup_directory, FALSE);
	
	/*set working directory*/
	SetCurrentDirectoryW(setup_directory);
	
	/*now actually launch the executable*/
	const DWORD exit_code = exec_shell_await(&banner, NULL, file_name, setup_absolute_path, lpCmdLine, setup_directory);
	
	/*clean-up*/
	if(banner != NULL)
	{
		DestroyWindow(banner);
	}
	
	ExitProcess(exit_code);
}

void CALLBACK startup(HWND hwnd, HINSTANCE hAppInstance, LPCSTR lpCmdLine, int nCmdShow)
{
	const wchar_t *const lpCmdLineW = get_next_arg(GetCommandLineW(), NULL, 0U);
	if(!EMPTY(lpCmdLineW))
	{
		startupW(hwnd, hAppInstance, (LPWSTR)get_next_arg(lpCmdLineW, NULL, 0U), nCmdShow);
	}
	else
	{
		startupW(hwnd, hAppInstance, L""/*no arguments*/, nCmdShow);
	}
}

void CALLBACK aboutW(HWND hwnd, HINSTANCE hAppInstance, LPCWSTR lpCmdLine, int nCmdShow)
{
	show_about(hwnd, TRUE);
}

void CALLBACK about(HWND hwnd, HINSTANCE hAppInstance, LPCSTR lpCmdLine, int nCmdShow)
{
	show_about(hwnd, TRUE);
}

#endif //BUILD_DLL
